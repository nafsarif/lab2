package edu.ucsd.cs110w.temperature;

public class Celsius extends Temperature{

	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		String ret = Float.toString(this.getValue());
		return " "+ret+" C ";
	}
	@Override
	public Temperature toCelsius() {
		float input = this.getValue();
		return new Celsius(input);
	}
	@Override
	public Temperature toFahrenheit() {
		float input = this.getValue();
		input*= 1.8;
		input+= 32;
		return new Fahrenheit(input);
	}
	@Override
	public Temperature toKelvin() {
		float input = this.getValue();
		input+= 273.15;
		return new Kelvin(input);
	}

}
