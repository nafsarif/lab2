package edu.ucsd.cs110w.temperature;

public class Fahrenheit extends Temperature{

	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		String ret = Float.toString(this.getValue());
		return ret+" F";
	}
	@Override
	public Temperature toCelsius() {
		float input = this.getValue();
		input-= 32;
		input/= 1.8;
		return new Celsius(input);
	}
	@Override
	public Temperature toFahrenheit() {
		float input = this.getValue();
		return new Fahrenheit(input);
	}
	@Override
	public Temperature toKelvin() {
		float input = this.getValue();
		input-= 32;
		input/= 1.8;
		input+= 273.15;
		return new Kelvin(input);
	}
}
